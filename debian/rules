#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS=hardening=+all
export CMAKE_PREFIX_PATH=/usr/lib/$(DEB_HOST_MULTIARCH)/cmake/
DTMP=$(CURDIR)/debian/sortmerna

# In this particular instance, building with sse2 support is not an
# architecture baseline violation for the i386 release architecture, because
# the package is supposed to depend on sse2-support.
ifeq ($(DEB_HOST_ARCH),i386)
export DEB_CFLAGS_MAINT_APPEND+=-msse2
export DEB_CXXFLAGS_MAINT_APPEND+=-msse2
endif

%:
	dh $@ --buildsystem=pybuild

execute_after_dh_install:
	find debian -name cmake -type d | xargs rm -rvf

execute_after_dh_installexamples:
	# Convert example files to Unicode.
	set -e \
	; cd $(CURDIR)/tests/sortmerna/ \
	; for cxxfile in *.cpp \
	; do iconv -f ISO-8859-15 -t UTF-8 \
	     < $${cxxfile} \
	     > $(DTMP)/usr/share/doc/sortmerna/examples/$${cxxfile} \
	; done

execute_after_dh_fixperms:
	find debian/sortmerna -name '*.sh' -exec chmod -v +x '{}' ';'
	find debian/sortmerna -name '*.py' -exec chmod -v -x '{}' ';'
