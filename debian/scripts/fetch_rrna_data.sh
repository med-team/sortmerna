#!/bin/bash
set -e
set -u

# I can't ship all the reference data in the DEB package.  This script
# will help you install it.

# Hopefully soon I'll be able to deal with this by integrating with BioMaj3

enterkey() {
  if tty -s ; then
      read -p "Press ENTER to continue, Ctrl+C to exit..." -r ignored
      echo
  fi
}

datadir=rRNA_databases

if echo "$*" | grep -wq -- --here ; then
    echo "Downloading to ./$datadir as --here was specified."
    # No prompt needed here.
    mkdir -p $datadir
    cd $datadir
elif [ "`id -u`" = 0 ] ; then
    datadir=/usr/share/sortmerna/rRNA_databases
    echo "Data will be downloaded to $datadir"
    cd $datadir
    umask 022
    enterkey
else
    datadir="$HOME/sortmerna/rRNA_databases"
    echo "Data will be downloaded to $datadir"
    enterkey
    mkdir -p "$datadir"
    cd "$datadir"
fi

#Let us fetch!  Thankfully no funny processing like we had to do with RDP-Classifier

#tag=master
tag=2.0

for db in rfam-{5s,5.8s}-database-id98  \
	  silva-arc-{16s-id95,23s-id98} \
	  silva-bac-{16s-id90,23s-id98} \
	  silva-euk-{18s-id95,28s-id98} ; do
    wget -c "https://github.com/biocore/sortmerna/raw/$tag/rRNA_databases/${db}.fasta"
done

echo DONE
